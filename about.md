---
layout: default
---

Merhaba, ben Murat, 2010 Haziran'ından bugüne web geliştiriciliği ile ilgileniyorum. Serüvenime PHP programlama dili ile başladım, ardından JavaScript, Less, Sass vb front-end teknolojilerine yöneldim, 2016 ortalarında Ruby programlama dili ile projeler geliştirmeye başladım, 2018 başından itibaren Elixir diline ve fonksiyonel programlama paradigmasını öğrenmeye başladım. Bugün aktif olarak Ruby ve Elixir dillerini kullanarak projeler geliştiriyorum.
